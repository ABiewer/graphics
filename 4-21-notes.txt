ways to increase detail:
1. add detail -> geometry shader, cannot do this in webgl
2. normal maps
3. bump maps

bump maps
vertex shader has a height map which give deltas in heights between vertexes so that proper heights can be shown on screen 
The height map still has three coordinates for each pixel/vertex, just that instead of RGB, it's delta x, delta y, and delta z

still uses 0-1 as the max/min values, just that 0.5 is a delta of 0, -0.5 is the maximum delta into the page in z-direction and +0.5 the maximum upwards. 


normal mapping
changes the values of the normals at a surface to simulate a rough texture, such as a brick wall
using same method as bump maps, changes in the normal represented by value from 0 to 1 and lighting is calculated from that. 

alpha compositing

RGBA - a for alpha or transparency
0 - transparent
1 - opaque

Compositing Algebra
Porter-Duff Algebra (Over compositing)
solves "compositing problem"

(presumably just mutliply RGB by the alpha value to get the value)

convering a pixel
(1-a_a)*(1-a_b) = background + 
